import React from "react"
import { Button as MuiButton, ButtonProps } from "@material-ui/core"
import { Button } from "../ButtonInterface"

const OkButton: React.FunctionComponent<ButtonProps & Button> = props => {
  const {
    variant = "contained",
    color = "primary",
    onClick = () => {
      console.log("clicked")
    },
  } = props
  return (
    <MuiButton onClick={onClick} {...props}>
      {props.children}
    </MuiButton>
  )
}

export default OkButton
