export interface Button {
  children?: any
  onClick?:
    | void
    | ((event: React.MouseEvent<HTMLButtonElement, MouseEvent>) => void)
}
