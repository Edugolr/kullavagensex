import React from "react"
import { Button as MuiButton, ButtonProps } from "@material-ui/core"

interface Button {
  children: any
  onClick:
    | void
    | ((event: React.MouseEvent<HTMLButtonElement, MouseEvent>) => void)
}

const CancelButton: React.FunctionComponent<ButtonProps> = props => {
  const {
    variant = "contained",
    color = "secondary",
    onClick = () => {
      console.log("clicked")
    },
  } = props
  return (
    <MuiButton onClick={onClick} variant={variant} color={color}>
      {props.children}
    </MuiButton>
  )
}

export default CancelButton
