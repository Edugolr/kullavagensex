import React from "react"

import { Typography } from "@material-ui/core"

const PageTitle: React.FunctionComponent = props => {
  return <Typography variant="h6">{props.children}</Typography>
}

export default PageTitle
