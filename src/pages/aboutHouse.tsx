// Gatsby supports TypeScript natively!
import React from "react"
import { PageProps, useStaticQuery, graphql } from "gatsby"
import Layout from "../components/layout"
import SEO from "../components/seo"
import {
  Grid,
  Card,
  CardActionArea,
  CardContent,
  Typography,
  CardActions,
  Button,
  Paper,
} from "@material-ui/core"
import PageTitle from "../atoms/Text/PageTitle/PageTitle"
import Img from "gatsby-image"

const aboutHouse: React.FunctionComponent = (props: PageProps) => {
  const data = useStaticQuery(graphql`
    query {
      placeholderImage: file(relativePath: { eq: "gatsby-astronaut.png" }) {
        childImageSharp {
          fixed(width: 200, height: 200) {
            ...GatsbyImageSharpFixed
          }
          fluid(maxWidth: 1000) {
            ...GatsbyImageSharpFluid
          }
        }
      }
      theHouse: file(relativePath: { eq: "huset.jpg" }) {
        childImageSharp {
          fixed(width: 400, height: 400) {
            ...GatsbyImageSharpFixed
          }
          fluid(maxWidth: 1000) {
            ...GatsbyImageSharpFluid
          }
        }
      }
    }
  `)
  return (
    <Layout>
      <SEO title="House" />
      <Grid container justify="center" alignItems="center">
        <PageTitle>Huset</PageTitle>
      </Grid>
      <Grid container justify="center" alignItems="center">
        <p>Här kommer information om huset</p>
      </Grid>
      <Grid
        container
        spacing={3}
        direction="row"
        justify="center"
        alignItems="flex-start"
      >
        <Grid item>
          <Paper elevation={1}>
            <Img
              title="House image"
              alt="Image on house"
              fixed={data.theHouse.childImageSharp.fixed}
            />
          </Paper>
        </Grid>
      </Grid>
    </Layout>
  )
}

export default aboutHouse
