// Gatsby supports TypeScript natively!
import React from "react"
import { PageProps, Link, useStaticQuery, graphql } from "gatsby"
import Layout from "../components/layout"
import SEO from "../components/seo"
import { Theme, createStyles, makeStyles } from "@material-ui/core/styles"
import GridList from "@material-ui/core/GridList"
import GridListTile from "@material-ui/core/GridListTile"
import GridListTileBar from "@material-ui/core/GridListTileBar"
import IconButton from "@material-ui/core/IconButton"
import StarBorderIcon from "@material-ui/icons/StarBorder"
import Img from "gatsby-image"
import { Grid } from "@material-ui/core"
import PageTitle from "../atoms/Text/PageTitle/PageTitle"

const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    root: {
      display: "flex",
      flexWrap: "wrap",
      justifyContent: "space-around",
      overflow: "hidden",
      backgroundColor: theme.palette.background.paper,
    },
    gridList: {
      display: "flex",
      flexWrap: "wrap",
      justifyContent: "space-around",
      overflow: "hidden",
      backgroundColor: theme.palette.background.paper,
      // Promote the list into his own layer on Chrome. This cost memory but helps keeping high FPS.
      transform: "translateZ(0)",
    },
    title: {
      color: "gold",
    },
    titleBar: {
      background:
        "linear-gradient(to top, rgba(0,0,0,0.7) 0%, rgba(0,0,0,0.3) 70%, rgba(0,0,0,0) 100%)",
    },
  })
)

const Pictures: React.FunctionComponent = props => {
  const classes = useStyles()
  const data = useStaticQuery(graphql`
    query {
      placeholderImage: file(relativePath: { eq: "gatsby-astronaut.png" }) {
        childImageSharp {
          fixed(width: 200, height: 200) {
            ...GatsbyImageSharpFixed
          }
          fluid(maxWidth: 1000) {
            ...GatsbyImageSharpFluid
          }
        }
      }
      theHouse: file(relativePath: { eq: "huset.jpg" }) {
        childImageSharp {
          fixed(width: 400, height: 400) {
            ...GatsbyImageSharpFixed
          }
          fluid(maxWidth: 1000) {
            ...GatsbyImageSharpFluid
          }
        }
      }
      sid: file(relativePath: { eq: "sid.jpg" }) {
        childImageSharp {
          fixed(width: 200, height: 200) {
            ...GatsbyImageSharpFixed
          }
          fluid(maxWidth: 1000) {
            ...GatsbyImageSharpFluid
          }
        }
      }
      rocky: file(relativePath: { eq: "rocky.jpg" }) {
        childImageSharp {
          fixed(width: 200, height: 200) {
            ...GatsbyImageSharpFixed
          }
          fluid(maxWidth: 1000) {
            ...GatsbyImageSharpFluid
          }
        }
      }
      parichat: file(relativePath: { eq: "parichat.jpg" }) {
        childImageSharp {
          fixed(width: 200, height: 200) {
            ...GatsbyImageSharpFixed
          }
          fluid(maxWidth: 1000) {
            ...GatsbyImageSharpFluid
          }
        }
      }
      christofer: file(relativePath: { eq: "christofer.jpg" }) {
        childImageSharp {
          fixed(width: 200, height: 200) {
            ...GatsbyImageSharpFixed
          }
          fluid(maxWidth: 1000) {
            ...GatsbyImageSharpFluid
          }
        }
      }
      ziggy: file(relativePath: { eq: "ziggy.jpg" }) {
        childImageSharp {
          fixed(width: 200, height: 200) {
            ...GatsbyImageSharpFixed
          }
          fluid(maxWidth: 330) {
            ...GatsbyImageSharpFluid
          }
        }
      }
    }
  `)

  const tileData = [
    {
      img: data.ziggy.childImageSharp.fluid,
      title: "Katten spanar",
      author: "Christofer",
      cols: 1,
      rows: 2,
    },
    {
      img: data.sid.childImageSharp.fluid,
      title: "Sid posar",
      author: "Parichat",
      cols: 1,
      rows: 2,
    },
    {
      img: data.theHouse.childImageSharp.fluid,
      title: "Någon annans hus",
      author: "Christofer",
      cols: 1,
      rows: 2,
    },
    {
      img: data.rocky.childImageSharp.fluid,
      title: "Fan man",
      author: "Parichat",
      colse: 1,
      rows: 1,
    },
    {
      img: data.parichat.childImageSharp.fluid,
      title: "Vackrare",
      author: "Parichat",
      cols: 0.5,
      rows: 1,
    },
    {
      img: data.christofer.childImageSharp.fluid,
      title: "Dags att uppdatera bild",
      author: "Christofer",
      cols: 1,
      rows: 2,
    },
  ]
  return (
    <Layout>
      <SEO title="Page two" />
      <Grid container justify="center" alignItems="center">
        <PageTitle>Bilder</PageTitle>
      </Grid>
      <Grid container justify="center" alignItems="center">
        <p>Här kommer det bilder</p>
      </Grid>
      <GridList className={classes.gridList} cols={3}>
        {tileData.map(tile => (
          <GridListTile
            key={tile.img}
            cols={tile.cols || 1}
            rows={tile.rows || 1}
          >
            <Img title={tile.title} alt={tile.title} fluid={tile.img} />

            <GridListTileBar
              title={tile.title}
              classes={{
                root: classes.titleBar,
                title: classes.title,
              }}
              actionIcon={
                <IconButton aria-label={`star ${tile.title}`}>
                  <StarBorderIcon className={classes.title} />
                </IconButton>
              }
            />
          </GridListTile>
        ))}
      </GridList>
    </Layout>
  )
}

export default Pictures
