// Gatsby supports TypeScript natively!
import React from "react"
import { PageProps, useStaticQuery, graphql } from "gatsby"
import Layout from "../components/layout"
import SEO from "../components/seo"
import {
  Card,
  CardActionArea,
  CardMedia,
  CardContent,
  Typography,
  CardActions,
  Button,
  Grid,
} from "@material-ui/core"
import PageTitle from "../atoms/Text/PageTitle/PageTitle"
import Img from "gatsby-image"

const aboutFamily: React.FunctionComponent = (props: PageProps) => {
  const data = useStaticQuery(graphql`
    query {
      placeholderImage: file(relativePath: { eq: "gatsby-astronaut.png" }) {
        childImageSharp {
          fixed(width: 200, height: 200) {
            ...GatsbyImageSharpFixed
          }
          fluid(maxWidth: 1000) {
            ...GatsbyImageSharpFluid
          }
        }
      }
      sid: file(relativePath: { eq: "sid.jpg" }) {
        childImageSharp {
          fixed(width: 200, height: 200) {
            ...GatsbyImageSharpFixed
          }
          fluid(maxWidth: 1000) {
            ...GatsbyImageSharpFluid
          }
        }
      }
      rocky: file(relativePath: { eq: "rocky.jpg" }) {
        childImageSharp {
          fixed(width: 200, height: 200) {
            ...GatsbyImageSharpFixed
          }
          fluid(maxWidth: 1000) {
            ...GatsbyImageSharpFluid
          }
        }
      }
      parichat: file(relativePath: { eq: "parichat.jpg" }) {
        childImageSharp {
          fixed(width: 200, height: 200) {
            ...GatsbyImageSharpFixed
          }
          fluid(maxWidth: 1000) {
            ...GatsbyImageSharpFluid
          }
        }
      }
      christofer: file(relativePath: { eq: "christofer.jpg" }) {
        childImageSharp {
          fixed(width: 200, height: 200) {
            ...GatsbyImageSharpFixed
          }
          fluid(maxWidth: 1000) {
            ...GatsbyImageSharpFluid
          }
        }
      }
      ziggy: file(relativePath: { eq: "ziggy.jpg" }) {
        childImageSharp {
          fixed(width: 200, height: 200) {
            ...GatsbyImageSharpFixed
          }
          fluid(maxWidth: 1000) {
            ...GatsbyImageSharpFluid
          }
        }
      }
    }
  `)
  return (
    <Layout>
      <SEO title="Familjen" />
      <Grid container justify="center" alignItems="center">
        <PageTitle>Familjen</PageTitle>
      </Grid>
      <Grid container justify="center" alignItems="center">
        <p>Här kommer presentation av familjen</p>
      </Grid>
      <Grid
        container
        spacing={3}
        direction="row"
        justify="center"
        alignItems="flex-start"
      >
        <Grid item>
          <Card style={{ maxWidth: 200 }}>
            <CardActionArea>
              <Img
                title="Header image"
                alt="Greek food laid out on table"
                fixed={data.sid.childImageSharp.fixed}
              />
              <CardContent>
                <Typography gutterBottom variant="h5" component="h2">
                  Sid
                </Typography>
                <Typography variant="body2" color="textSecondary" component="p">
                  Sid är en streamer
                </Typography>
              </CardContent>
            </CardActionArea>
            <CardActions>
              <Button size="small" color="primary">
                Share
              </Button>
              <Button size="small" color="primary">
                Learn More
              </Button>
            </CardActions>
          </Card>
        </Grid>
        <Grid item>
          <Card style={{ maxWidth: 200 }}>
            <CardActionArea>
              <Img
                title="Header image"
                alt="Greek food laid out on table"
                fixed={data.rocky.childImageSharp.fixed}
              />
              <CardContent>
                <Typography gutterBottom variant="h5" component="h2">
                  Rocky
                </Typography>
                <Typography variant="body2" color="textSecondary" component="p">
                  Rocky är grafiker
                </Typography>
              </CardContent>
            </CardActionArea>
            <CardActions>
              <Button size="small" color="primary">
                Share
              </Button>
              <Button size="small" color="primary">
                Learn More
              </Button>
            </CardActions>
          </Card>
        </Grid>
        <Grid item>
          <Card style={{ maxWidth: 200 }}>
            <CardActionArea>
              <Img
                title="Header image"
                alt="Greek food laid out on table"
                fixed={data.parichat.childImageSharp.fixed}
              />
              <CardContent>
                <Typography gutterBottom variant="h5" component="h2">
                  Parichat
                </Typography>
                <Typography variant="body2" color="textSecondary" component="p">
                  Parichat är en växtgäri
                </Typography>
              </CardContent>
            </CardActionArea>
            <CardActions>
              <Button size="small" color="primary">
                Share
              </Button>
              <Button size="small" color="primary">
                Learn More
              </Button>
            </CardActions>
          </Card>
        </Grid>
        <Grid item>
          <Card style={{ maxWidth: 200 }}>
            <CardActionArea>
              <Img
                title="Header image"
                alt="Greek food laid out on table"
                fixed={data.christofer.childImageSharp.fixed}
              />
              <CardContent>
                <Typography gutterBottom variant="h5" component="h2">
                  Christofer Wikman
                </Typography>
                <Typography variant="body2" color="textSecondary" component="p">
                  Christofer är en
                </Typography>
              </CardContent>
            </CardActionArea>
            <CardActions>
              <Button size="small" color="primary">
                Share
              </Button>
              <Button size="small" color="primary">
                Learn More
              </Button>
            </CardActions>
          </Card>
        </Grid>
        <Grid item>
          <Card style={{ maxWidth: 200 }}>
            <CardActionArea>
              <Img
                title="Header image"
                alt="Greek food laid out on table"
                fixed={data.ziggy.childImageSharp.fixed}
              />
              <CardContent>
                <Typography gutterBottom variant="h5" component="h2">
                  Ziggy
                </Typography>
                <Typography variant="body2" color="textSecondary" component="p">
                  Ziggy är en katt
                </Typography>
              </CardContent>
            </CardActionArea>
            <CardActions>
              <Button size="small" color="primary">
                Share
              </Button>
              <Button size="small" color="primary">
                Learn More
              </Button>
            </CardActions>
          </Card>
        </Grid>
        <Grid item>
          <Card style={{ maxWidth: 200 }}>
            <CardActionArea>
              <CardMedia
                style={{ height: 140 }}
                image="/static/images/cards/contemplative-reptile.jpg"
                title="Contemplative Reptile"
              />
              <CardContent>
                <Typography gutterBottom variant="h5" component="h2">
                  Billy
                </Typography>
                <Typography variant="body2" color="textSecondary" component="p">
                  Billy är en robotdamsugare
                </Typography>
              </CardContent>
            </CardActionArea>
            <CardActions>
              <Button size="small" color="primary">
                Share
              </Button>
              <Button size="small" color="primary">
                Learn More
              </Button>
            </CardActions>
          </Card>
        </Grid>
      </Grid>
    </Layout>
  )
}

export default aboutFamily
